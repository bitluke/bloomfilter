package com.bfs;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * A class to read text from dictionary.txt (content from http://codekata.com/data/wordlist.txt)
 * file in the resource directory.
 */
public class DictionaryReader {

    static final Logger LOGGER = Logger.getLogger(DictionaryReader.class.getSimpleName());

    public List<String> readFile(String filePath) {
        List<String> words = new ArrayList<String>();
        try (BufferedReader bufferedReader =
                     new BufferedReader(new FileReader(filePath))) {
            words = readFile(bufferedReader);
        }catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "File which You loking for not found! :" + e);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "File can't be read :" + e);
        }
        return words;
    }

    public String getDefaultFilePath() {
        return getClass().getClassLoader().getResource("dictionary.txt").getPath();
    }


    public  List<String> readFile(BufferedReader bufferedReader) throws IOException {
        String wordsPerLine = "";
        List<String> words = new ArrayList<String>();
        while ((wordsPerLine = bufferedReader.readLine()) != null) {
            words.add(wordsPerLine);
        }
        return words;
    }
}
