package com.bfs;

/**
 * An interface  any hash function should implement to use
 * within the bloom filter . Different type of hashes like
 * Linear congruent generator , pseudo random number generators
 * Mummur hash , city hash , Jenkins hash.
 */
public interface HashType  {
   int[] hash(int numberofHashes, int maxValue, byte[] str);
}
