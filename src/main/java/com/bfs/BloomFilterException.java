package com.bfs;
/**
 *Exception thrown when a wrong bloom filter is set up.
 */
public class BloomFilterException extends RuntimeException {
    public BloomFilterException() {
        super();
    }

    public BloomFilterException(String s) {
        super(s);
    }
}
