package com.bfs;


import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * A simple implementation of a Bloom filter interface . The implementation depends
 * on bitsets which makes it efficient as compared to using array/list of booleans.
 * A BloomConfigBuilder is needed to instantiate the bloom Filter implementation object.
 */
public class BloomFilterImpl implements BloomFilter {
    private  static final Logger LOGGER = Logger.getLogger(BloomFilterImpl.class.getSimpleName());
    private BitSet filterBits;
    private final BloomConfigBuilder configuration;

    public BloomFilterImpl(BloomConfigBuilder configuration) {
        configuration.build();
        filterBits = new BitSet(configuration.size());
        this.configuration = configuration;
    }


    public BloomConfigBuilder getConfiguration() {
        return configuration;
    }

    @Override
    public void clear() {
        filterBits.clear();
    }

    @Override
    public boolean add(byte[] str) {
        boolean added = false;
        for (int index : hash(str)) {
            if (!getBit(index)) {
                added = true;
                setBit(index, true);
            }
        }
        return added;
    }

    public List<Boolean> addAll(Collection<String> elements) {
        List<Boolean> items = new ArrayList<Boolean>(elements.size());
        for (String str : elements) {
            boolean val = add(str);
            items.add(val);
        }
        return items;
    }

    public boolean add(String str) {
        return add(getBytes(str));
    }

    public List<Boolean> contains(Collection<String> elements) {
        List<Boolean> items = new ArrayList<Boolean>(elements.size());
        for (String str : elements) {
            boolean val = contains(str);
            items.add(val);
        }
        return items;
    }


    public boolean contains(String str) {
        return contains(getBytes(str));
    }

    @Override
    public boolean contains(byte[] str) {
        for (int index : hash(str)) {
            if (!getBit(index)) {
                return false;
            }
        }
        return true;
    }


    public byte[] getBytes(String str) {
        return str.getBytes(BloomConfigBuilder.getBaseCharset());
    }


    protected boolean getBit(int index) {
        return filterBits.get(index);
    }

    protected void setBit(int index, boolean to) {
        filterBits.set(index, to);
    }

    @Override
    public BitSet getBitSet() {
        return filterBits;
    }


    @Override
    public boolean isEmpty() {
        return filterBits.isEmpty();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (!(o instanceof BloomFilterImpl)) {return false;}
        BloomFilterImpl that = (BloomFilterImpl) o;
        return !(filterBits != null ? !filterBits.equals(that.filterBits) : that.filterBits != null);
    }

    @Override
    public int hashCode() {
        return filterBits.hashCode();
    }

    public int[] hash(String value) {
        return hash(value.getBytes(BloomConfigBuilder.getBaseCharset()));
    }


    public int[] hash(byte[] bytes) {
        return getConfiguration().hashType().hash(getConfiguration().hashes(), getConfiguration().size(), bytes);
    }


    public double getFalsePositiveProbability(double insertedElements) {
        return BloomConfigBuilder.getBestFalsePositiveProbability(getConfiguration().hashes(), getConfiguration().size(), insertedElements);
    }


    public double getApproxFalsePositiveProbability() {
        return getFalsePositiveProbability(approximateSize());
    }


    public double getBitsPerElement(int n) {
        return getConfiguration().size() / (double) n;
    }


    public Double approximateSize() {
        return calculateSize(getBitSet(), getConfiguration());
    }

    public static Double calculateSize(BitSet bits, BloomConfigBuilder cfg) {
        int cardinality = bits.cardinality();
        return (-1 * cfg.size()) / ((double) cfg.hashes()) * Math.log(1 - cardinality / ((double) cfg.size()));
    }


    @Override
    public String toString() {
        return "BloomFilterImpl{" +
                "hashes=" + configuration.hashes() +
                ", approximateSize=" + configuration.size() +
                ", filterBits=" + filterBits +
                '}';
    }
}
