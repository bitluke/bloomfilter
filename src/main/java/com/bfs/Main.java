package com.bfs;


import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * A sample class to show how the bloom filter works .
 */
public class Main {
    static final Logger LOGGER = Logger.getLogger(Main.class.getSimpleName());

    public static void main(String[] args) {

        DictionaryReader dictionaryReader = new DictionaryReader();
        List<String> words = dictionaryReader.readFile(dictionaryReader.getDefaultFilePath());


        BloomFilter memoryBloomFilter =
                new BloomConfigBuilder(1000, 0.1)
                        .hashType(new MD5Hash())
                        .hashes(4)
                        .buildBloomFilterImpl();

        StringBuilder stringOffalsePositives = new StringBuilder();
        for (int i = 0; i < 290; i++) {
            String word = words.get(i);
            memoryBloomFilter.add(word.getBytes());
            stringOffalsePositives.append(word).append(",");
        }

        LOGGER.info("Added the following to the filter: " +
                stringOffalsePositives.toString()
                        .substring(0, stringOffalsePositives.length() - 1) );


        stringOffalsePositives.delete(0, stringOffalsePositives.length());
        for (int i = 300; i < 970; i++) {
            String word = words.get(i);
            if (memoryBloomFilter.contains(word.getBytes())) {
                stringOffalsePositives.append(word).append(",");
            }
        }

        LOGGER.info("list of false positives: " +
                stringOffalsePositives.toString()
                        .substring(0, stringOffalsePositives.length() - 1));
    }
}
