package com.bfs;


import java.util.BitSet;
import java.util.Collection;
import java.util.List;

/**
 * The Base interface used for extension in order to build a
 * Bloom filter.
 */
public interface BloomFilter {
    List<Boolean> addAll(Collection<String> elements);

    boolean add(String str);

    boolean add(byte[] element);

    void clear();

    List<Boolean> contains(Collection<String> elements);

    boolean contains(String str);

    boolean contains(byte[] element);

    BitSet getBitSet();

    BloomConfigBuilder getConfiguration();

    double getApproxFalsePositiveProbability();

    boolean isEmpty();

    Double approximateSize();
}
