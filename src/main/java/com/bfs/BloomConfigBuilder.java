package com.bfs;


import java.nio.charset.Charset;
import java.util.logging.Logger;

/**
 *This class is used to build the configuration of a Bloom Filter .
 * Other parameters of the Bloom filters can be derived within the
 * BloomConfigBuilder object.
 */
public class BloomConfigBuilder {
    private static final Logger LOGGER = Logger.getLogger(BloomConfigBuilder.class.getSimpleName());
    private Integer expectedElements;
    private Integer size;
    private static Charset defaultCharset = Charset.forName("UTF-8");
    private boolean finished = false;
    private Integer numberOfhashFunctions;
    private Double falsePositiveProb;
    private HashType hashType = new MD5Hash();


    public BloomConfigBuilder() {
    }

    public BloomConfigBuilder(int expectedElements, double falsePositiveProb) {
        if (expectedElements <= 0) {
            throw new IllegalArgumentException("Expected Number of Element should be greater than 0 ");
        }
        if (falsePositiveProb < 0.0d || falsePositiveProb > 1.0d) {
            throw new IllegalArgumentException("probability should be between 0 and 1");
        }
        this.expectedElements(expectedElements).falsePositiveProbability(falsePositiveProb);
    }

    public BloomConfigBuilder(int size, int numberOfhashFunctions) {
        if (size <= 0) {
            throw new IllegalArgumentException("Size should be greater than 0 ");
        }
        if (numberOfhashFunctions <= 0) {
            throw new IllegalArgumentException("Hashes should be greater than 0 ");
        }
        this.size(size).hashes(numberOfhashFunctions);
    }

    public BloomConfigBuilder expectedElements(int expectedElements) {
        this.expectedElements = expectedElements;
        return this;
    }

    public BloomConfigBuilder size(int size) {
        this.size = size;
        return this;
    }

    public BloomConfigBuilder falsePositiveProbability(double falsePositiveProbability) {
        this.falsePositiveProb = falsePositiveProbability;
        return this;
    }


    public BloomConfigBuilder hashes(int numberOfHashes) {
        this.numberOfhashFunctions = numberOfHashes;
        return this;
    }

    public BloomConfigBuilder hashType(HashType hashType) {
        this.hashType = hashType;
        return this;
    }


    public BloomFilter buildBloomFilterImpl() {
        build();
        return new BloomFilterImpl(this);
    }

    public BloomConfigBuilder build() {
        if (finished) {
            return this;
        }
        if (size == null && expectedElements != null && falsePositiveProb != null) {
            size = getBestSizeInBits(expectedElements, falsePositiveProb);
        }
        if (numberOfhashFunctions == null && expectedElements != null && size != null) {
            numberOfhashFunctions = getBestNumberOfHashFunctions(expectedElements, size);
        }
        if (size == null || numberOfhashFunctions == null) {
            LOGGER.severe("Cannot use the available parameters to build a filter.");
            throw new BloomFilterException("Use any of the non default constructors, approximateSize or number of hashes can't be null ");
        }
        if (expectedElements == null) {
            expectedElements = getOptimumNumberOfElement(numberOfhashFunctions, size);
        }
        if (falsePositiveProb == null) {
            falsePositiveProb = getBestFalsePositiveProbability(numberOfhashFunctions, size, expectedElements);
        }
        finished = true;
        LOGGER.info("Bloom filter configuration has been built.");
        return this;
    }

    public int expectedElements() {
        return expectedElements;
    }

    public int size() {
        return size;
    }

    public static int getBestSizeInBits(long expectedNumberOfElements, double falsePositiveRate) {
        return (int) Math.ceil(-1 * ( Math.log(falsePositiveRate) * expectedNumberOfElements) / Math.pow(Math.log(2), 2));
    }

    public static int getOptimumNumberOfElement(long hashes, long sizeInBits) {
        return (int) Math.ceil((sizeInBits * Math.log(2) ) / hashes);
    }

    public boolean isFinished() {
        return finished;
    }

    public static int getBestNumberOfHashFunctions(long expectedNumberOfElements, long sizeInBits) {
        return (int) Math.ceil((Math.log(2) * sizeInBits) / expectedNumberOfElements);
    }

    public int hashes() {
        return numberOfhashFunctions;
    }

    public double falsePositiveProbability() {
        return falsePositiveProb;
    }

    public HashType hashType() {
        return hashType;
    }

    public static double getBestFalsePositiveProbability(long hashes, long sizeInBits, double numOfInsertedElements) {
        return Math.pow((1 - Math.exp(-hashes * numOfInsertedElements / (double) sizeInBits)), sizeInBits);
    }

    public static Charset getBaseCharset() {
        return defaultCharset;
    }

}
