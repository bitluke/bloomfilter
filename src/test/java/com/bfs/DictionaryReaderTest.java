package com.bfs;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

public class DictionaryReaderTest {
    private String[] initList = {
            "A'asia", " A's", "AA", "AA's", "AAA",
            "AAM", "AB", "AB's", "ABA", "ABC",
            "ABC's", "ABCs", "ABD", "ABDs", "ABM",
            "ABM's", "ABMs", "ABS", "AC", "AC's", "ACLU"};
    @Mock
    BufferedReader bufferedReader;


    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testReadFile() throws Exception {
        when(bufferedReader.readLine()).thenReturn("A'asia", "A's", "AA");
        assertEquals(bufferedReader.readLine(),"A'asia" );
        assertEquals(bufferedReader.readLine(),"A's" );
        assertEquals(bufferedReader.readLine(),"AA" );

    }
}
