package com.bfs;


import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.testng.Assert.*;

public class BloomFilterImplTest {
    private BloomFilterImpl memoryBloomFilter;
    private static final int NUMBER_ELEMENTS = 26;
    private static final double FALSE_POSITIVE_RATE = 0.01;
    private HashType MD5 = new MD5Hash();


    private List<String> initList = Arrays.asList(
            "A'asia", " A's", "AA", "AA's", "AAA",
            "AAM", "AB", "AB's", "ABA", "ABC",
            "ABC's", "ABCs", "ABD", "ABDs", "ABM",
            "ABM's", "ABMs", "ABS", "AC", "AC's", "ACLU");

    @Mock
    DictionaryReader dictionaryReader;

    @Mock
    BloomConfigBuilder bloomConfigBuilder;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        memoryBloomFilter = (BloomFilterImpl)
                new BloomConfigBuilder(NUMBER_ELEMENTS, FALSE_POSITIVE_RATE)
                        .hashType(MD5)
                        .buildBloomFilterImpl();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        memoryBloomFilter = null;
    }


    @Test
    public void shouldContainFirstFiveElements() {

        when(dictionaryReader.readFile(dictionaryReader.getDefaultFilePath()))
                .thenReturn(initList);


        List<String> words = dictionaryReader.readFile(dictionaryReader.getDefaultFilePath());

        for (int i = 0; i < 5; i++) {
            String word = words.get(i);
            memoryBloomFilter.add(word.getBytes());
        }

        for (int i = 0; i < 5; i++) {
            String word = words.get(i);
            assertTrue(memoryBloomFilter.contains(word.getBytes()));
        }
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void shouldThrowExceptionWithoutConstructorCall() {
        when(bloomConfigBuilder.expectedElements(NUMBER_ELEMENTS)
                .falsePositiveProbability(FALSE_POSITIVE_RATE).build())
                .thenReturn(bloomConfigBuilder);

        new BloomFilterImpl(bloomConfigBuilder);

    }

    @Test
    public void shouldCreateABloomFilter() {
        assertNotNull(new BloomFilterImpl(new BloomConfigBuilder(NUMBER_ELEMENTS, FALSE_POSITIVE_RATE)));
    }

    @Test
    public void shouldNotContainSixthToNinethElements() {
        when(dictionaryReader.readFile(dictionaryReader.getDefaultFilePath()))
                .thenReturn(initList);

        List<String> words = dictionaryReader.readFile(dictionaryReader.getDefaultFilePath());

        for (int i = 0; i < 5; i++) {
            String word = words.get(i);
            memoryBloomFilter.add(word.getBytes());
        }

        for (int i = 6; i < 9; i++) {
            String word = words.get(i);
            assertTrue(!memoryBloomFilter.contains(word.getBytes()));
        }
    }


    @Test
    public void shouldAddAnElementAtByteLevel() {
        assertTrue(memoryBloomFilter.add(initList.get(0).getBytes()));
    }

    @Test
    public void shouldAddAnElementAtStringLevel() {
        assertTrue(memoryBloomFilter.add(initList.get(0)));
    }

    @Test
    public void shouldAddAnElementAtCollectionLevel() {
        memoryBloomFilter.addAll(initList.subList(0, 5));
        assertEquals((memoryBloomFilter.approximateSize()).intValue(), 5);
    }

    @Test
    public void shouldContainAnElement() {
        memoryBloomFilter.add(initList.get(0).getBytes());
        assertEquals((memoryBloomFilter.approximateSize()).intValue(), 1);
    }

    @Test
    public void testContainAtElementLevel() {
        memoryBloomFilter.add(initList.get(0));
        assertTrue(memoryBloomFilter.contains(initList.get(0)));
    }

    @Test
    public void testContainAtBulkLevel() {
        memoryBloomFilter.addAll(initList.subList(0,2));
        assertTrue(memoryBloomFilter.contains(initList.subList(0, 2)).get(0));
        assertTrue(memoryBloomFilter.contains(initList.subList(0,2)).get(1));
    }

    @Test
    public void testContainAtByteLevel() {
        memoryBloomFilter.add(initList.get(0).getBytes());
        assertTrue(memoryBloomFilter.contains(initList.get(0).getBytes()));
    }

    @Test
    public void shouldBeEmpty() {
        assertEquals(Math.round(memoryBloomFilter.approximateSize()), 0);
    }

    @Test
    public void shouldClearFilter() {
        for (int i = 0; i < 5; i++) {
            memoryBloomFilter.add(initList.get(i));
        }
        memoryBloomFilter.clear();
        assertEquals(Math.round(memoryBloomFilter.approximateSize()), 0);
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(memoryBloomFilter.isEmpty());
    }


    @DataProvider
    public Object[][] validInsertedElement() {
        return new Object[][]{
                {10},
                {13},
                {20},
        };
    }

    @Test(dataProvider = "validInsertedElement")
    public void probabilityShouldBetweenZeroAndOne(double insertedElements) throws Exception {
        assertTrue(memoryBloomFilter.getFalsePositiveProbability(insertedElements) > 0);
        assertTrue(memoryBloomFilter.getFalsePositiveProbability(insertedElements) < 1);
    }

    @Test(dataProvider = "validInsertedElement")
    public void probabilityShouldNotLessThanZeroAndGreaterThanOne(double insertedElements) throws Exception {
        assertFalse(memoryBloomFilter.getFalsePositiveProbability(insertedElements) < 0);
        assertFalse(memoryBloomFilter.getFalsePositiveProbability(insertedElements) > 1);
    }

    @Test
    public void testSizeShouldBeGreaterthanZero() throws Exception {
        memoryBloomFilter.addAll(initList);
        assertTrue(BloomFilterImpl.calculateSize(memoryBloomFilter.getBitSet(), memoryBloomFilter.getConfiguration()) > 0);
    }


}