package com.bfs;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MD5HashTest {

    MD5Hash md5;

    @BeforeMethod
    public void setUp() throws Exception {
        md5 = new MD5Hash();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        md5 = null;
    }

    @DataProvider
    public Object[][] validDataProvider() {
        return new Object[][]{
                {1, 1, new byte[]{2}},
                {1, 1, new byte[]{2, 2, 2, 2}},
        };
    }

    @Test(dataProvider = "validDataProvider")
    public void shouldCreateHashes(int numberofHashes, int maxValue, byte[] str) throws Exception {
        assertNotNull(md5.hash(numberofHashes, maxValue, str));
    }

    @DataProvider
    public Object[][] inValidDataProvider() {
        return new Object[][]{
                {-1, 1, new byte[]{6}},
                {1, 0, new byte[]{4}},
                {0, 1, new byte[]{8}},
                {1, -1, new byte[]{2}},
                {1, -1, null},
                {0, 0, null},
        };
    }

    @Test(dataProvider = "inValidDataProvider", expectedExceptions = {IllegalArgumentException.class})
    public void shouldThrowException(int numberofHashes, int maxValue, byte[] str) throws Exception {
        md5.hash(numberofHashes, maxValue, str);
    }
}