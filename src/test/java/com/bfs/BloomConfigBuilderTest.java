package com.bfs;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


public class BloomConfigBuilderTest {
    static final int NUMBER_OF_EXPECTED_ELEMENTS = 100;
    static final double FALSE_POSITIVE_RATE = 0.01;

    BloomConfigBuilder defaultBcf = new BloomConfigBuilder();
    BloomConfigBuilder bcfWithExpectedParam = new BloomConfigBuilder(NUMBER_OF_EXPECTED_ELEMENTS, FALSE_POSITIVE_RATE);

    @BeforeMethod
    public void setUp() throws Exception {
    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @DataProvider
    public Object[][] invalidSizeAndHash() {
        return new Object[][]{
                {-100, -1},
                {100, 0},
                {-100, 1},
                {-100, 2},
        };
    }

    @Test(dataProvider = "invalidSizeAndHash", expectedExceptions = {IllegalArgumentException.class})
    public void testInvalidSizeAndHashAtCreation(int size, int hashes) {
        new BloomConfigBuilder(size, hashes);
    }

    @DataProvider
    public Object[][] invalidExpectedSizeAndProbability() {
        return new Object[][]{
                {-100, -0.1},
                {100, -0.1},
                {-100, 0.1},
                {-100, 2.0},
        };
    }

    @Test(dataProvider = "invalidExpectedSizeAndProbability", expectedExceptions = {IllegalArgumentException.class})
    public void testInvalidExpectedSizeAndProbabilityAtCreation(int expectedSize, double probability) {
        new BloomConfigBuilder(expectedSize, probability);
    }


    @Test(expectedExceptions = {BloomFilterException.class})
    public void testShouldThrowExceptionWithDefault() {
        defaultBcf.build();
    }

    @Test
    public void testGetBestNumberOfHashFunctions() {
        assertEquals(bcfWithExpectedParam.getBestNumberOfHashFunctions(11, 88), 6);
    }

    @DataProvider
    public Object[][] invalidHashFunctionValues() {
        return new Object[][]{
                {11, 88, 216},
                {1, 88, 21},
                {11, 83, 16},
        };
    }

    @Test(dataProvider = "invalidHashFunctionValues")
    public void testInvalidGetBestNumberOfHashFunctions(long expectedNumberOfElements, long sizeInBits, long result) {
        assertNotEquals(bcfWithExpectedParam.getBestNumberOfHashFunctions(expectedNumberOfElements, sizeInBits), result);
    }


    @DataProvider
    public Object[][] invalidBestSizeInBitsValues() {
        return new Object[][]{
                {11, 0.1, 0},
                {0, 889, 21},
                {11, 83, 16},
        };
    }

    @Test(dataProvider = "invalidBestSizeInBitsValues")
    public void testInValidGetBestSizeInBits(long expectedNumberOfElements, double falsePositiveRate, int result) {
        assertNotEquals(bcfWithExpectedParam.getBestSizeInBits(expectedNumberOfElements, falsePositiveRate), result);
    }

    @Test
    public void testGetBestSizeInBits() {
        assertEquals(bcfWithExpectedParam.getBestSizeInBits(10, 0.01), 96);
    }


    @DataProvider
    public Object[][] invalidOptimumNumberOfElementValues() {
        return new Object[][]{
                {11, 88, 62},
                {1, 88, 36},
                {1, 82, 12},

        };
    }

    @Test(dataProvider = "invalidOptimumNumberOfElementValues")
    public void testinvalidOptimumNumberOfElement(long hashes, long sizeInBits, int result) {
        assertNotEquals(bcfWithExpectedParam.getOptimumNumberOfElement(hashes, sizeInBits), result);
    }

    @Test
    public void testGetOptimumNumberOfElement() {
        assertEquals(bcfWithExpectedParam.getOptimumNumberOfElement(11, 88), 6);
    }

    @DataProvider
    public Object[][] invalidFalsePositiveProbabilityValues() {
        return new Object[][]{
                {11, 88, 2, 3.4},
                {1, 34, 3, 2.2},
                {12, 32, 1332, 2.3},
        };
    }

    @Test(dataProvider = "invalidFalsePositiveProbabilityValues")
    public void testInvalidBestFalsePositiveProbability(long hashes, long sizeInBits, double numOfInsertedElements, double result) {
        assertNotEquals(bcfWithExpectedParam.getBestFalsePositiveProbability(hashes, sizeInBits, numOfInsertedElements), result);
    }

    @Test
    public void testGetBestFalsePositiveProbability() {
        assertEquals(bcfWithExpectedParam.getBestFalsePositiveProbability(1000, 8000, 10), 0.0);
    }

}