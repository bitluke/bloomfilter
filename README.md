# Bloom Filter. #

Bloom filter is an efficient data structure that determines the membership of an element within a collection of elements. It achieves the efficiency by using less amount of bits to store an element.

## Implementation details of a bloom filter. ##

A bird’s-eye view of the Bloom filter implementation is shown in the class diagram below :

![classdiagram.jpg](https://bitbucket.org/repo/eo8edM/images/2097437179-classdiagram.jpg)


From the figure 1 above there are 6 main classes and two interfaces that are involved in the creation of the Bloom filter. A detailed explanation of the each is given below:

### Interfaces ###

1. **BloomFilter** : This is the base interface that has to be implemented for the creation of a Bloom filter. Most basic methods of the Bloom filter are declared within the interface.

2. **HashType** : This is the base interface for the Bloom filter's hash function in a case where extension is needed for the Bloom filter. Other hash functions like Jenkins, Murmur and city hash can be configured for hashing.


### Classes ###
1.**Main**: The Main class demonstrates the basic usage of the Bloom filter API. Which means a configuration object is needed and then added to the filter which in turn is the queried for the  the existence of a particular element. This class also demonstrates the existence of a false positive within the implementation of a Bloom filter.

2.**BloomConfigBuilder** : This class is used to build the configuration for a Bloom Filter .
 Other parameters like false positive probability, number hashes to uses, size in bits of the Bloom filters can be derived within the BloomConfigBuilder object.

3. **BloomFilterImpl** : This is a simple implementation of the BloomFilter interface. It is configured via the BloomConfigBuilder object. Once configured basic functions like `add(), addAll(), contains(), containsAll()` can be carried out on the implementation based on the specified configuration at creation time

4. **MD5Hash** :is an implementation of HashType that is used within the implemented bloom filter by the default.

5. **DictionaryReader** : This class is used to read text from dictionary.txt (content from http://codekata.com/data/wordlist.txt) which is the resource directory of the project.

6. **BloomFilterException** :  Is an Exception thrown when a wrong Bloom filter is constructed.


### TestClasses ###
From the [diagram](https://www.dropbox.com/s/36b4cozn11cqz3z/classdiagram.jpg?dl=0) above, classes ending with Test suffix demonstrate the testing of each class within the implementation..


## Mode of Operation for a bloom filter. ##
In order to use the String type based implemented Bloom filter, a BloomConfigBuilder object is created with either a combination of expected number of elements and a false positive probability value or a combination of the size in bits and number of hashes to consider. The `buildBloomFilterImpl()` method of the BloomConfigBuilder is 
then called or passed to the constructor of a `BloomFilterImpl` constructor in order to create a BloomFilter implementation. After the creation of the Bloom filter a string can now be added to it via any of the two `add()` methods and one `addAll()`. When there is an addition to be made to the Bloom filter, the added string is converted into bytes and then hashed a number of times (as calculated by`BloomConfigBuilder`  during the setup) in order to determine the positions that would be active within the underlying bit sets of the Bloom filter. Other elements can now be added by following the same procedure. Elements can also be queried for within the Bloomfilter.

*  **False Positive probability** :
The implemented Bloom filter has the tendency to declare that an element is present when it is actually not present. This phenomenon is called false positivity. The phenomenon can be controlled by increasing the number of bits used for the implementation of the Bloom filter when it is been built. The `getBest*()` methods can be used to get the best configuration of the Bloom Filter. 

## Features and usages. ##
The implemented Bloom filter has the following characteristics :

### Creation ###

In order to make use of the Bloom filter. It is expected that a Bloom filter configuration should be created as shown below :
with number of expected elements and false positive probability :     

```
    new BloomConfigBuilder(1000, 0.01); 
```

or with size in bits and number hashes to carry out :

```
    new BloomConfigBuilder(800, 3); 
```

then a Bloom filter is then built either via the build method of the configuration object as shown below:
    
```
    new BloomConfigBuilder(800, 3).buildBloomFilterImpl();  
```

or via the constructor of a Bloom filter implementation :     

```
    BloomConfigBuilder bcf = new BloomConfigBuilder(800, 3);
    BloomFilter bloomfilter = new BloomFilterImpl(bcf); 
```
### Bloom filters parameters ###
The major properties of the Bloom filter can be ascertained from the configuration object BloomConfigBuilder as shown below:
Given :
```
    BloomConfigBuilder bcf = new BloomConfigBuilder(800, 3);
    long hashes = 2;
    double falsePositiveRate = 0.01;
    double numOfInsertedElements = 10;
    long expectedNumberOfElements = 1000;
    long sizeInBits = 8000;

```
one can :

 * Get the best size in bits based on the expected number of elements and false positive rate.

```
    bcf.getBestSizeInBits(expectedNumberOfElements,falsePositiveRate);
```

* Get the optimum number of Elements to use based on the hashes to carry out and size in bits.
```
    bcf.getOptimumNumberOfElement(hashes,sizeInBits);
```

* Get the best number of hashes to use based on the expected number of elements and size in bits.
```
    bcf.getBestNumberOfHashFunctions(expectedNumberOfElements, sizeInBits);
```
* Get the best False positive probability based on the number of hashes, size in bits, and the number of inserted elements. 
```
    bcf.getBestFalsePositiveProbability( hashes, sizeInBits, numOfInsertedElements)
```

### Adding a string element (word) at byte, text and collection level. ###
  
```
    private List<String> initList = Arrays.asList(
            "A'asia", " A's", "AA", "AA's", "AAA",
            "ABM's", "ABMs", "ABS", "AC", "AC's", "ACLU");

    bloomFilter.add(initList.get(0).getBytes()); //byte level
    bloomFilter.add(initList.get(0)); //string level
    bloomFilter.addAll(initList.subList(0, 5)); //collection level

```

### Checking for the existence of a string element (word) at byte, text and collection level. ###

    
```

    private List<String> initList = Arrays.asList(
            "A'asia", " A's", "AA", "AA's", "AAA",
            "ABM's", "ABMs", "ABS", "AC", "AC's", "ACLU");

    bloomFilter.contains(initList.get(0).getBytes());//byte level
    bloomFilter.contains(initList.get(0));//string level
    bloomFilter.contains(initList.subList(0,2);//collection level
```

### Clearing elements from the filter. ###

```
    bloomFilter.clear();
```
### Calculating the approximate Size (number of elements) ###

```
    bloomFilter.approximateSize();
```
### Calculating the optimal false positive probability ###

```
    int insertedElements = 12;
    bloomFilter.getFalsePositiveProbability(insertedElements) ;
```